FROM mcr.microsoft.com/dotnet/core/sdk:3.1

COPY . /server/

RUN [ "dotnet", "build", "-c", "Release", "/server/NotificationServer/" ]

ENTRYPOINT [ "dotnet", "run", "-c", "Release", "-p", "/server/NotificationServer/" ]

EXPOSE 30030
EXPOSE 30031