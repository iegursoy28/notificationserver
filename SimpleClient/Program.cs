﻿using System;
using System.Collections.Generic;
using CommandLine;
using Grpc.Core;
using System.Linq;

namespace SimpleClient
{
    class Program
    {
        static string _host;
        static int _port;
        static string _package;

        public class Options
        {
            [Option('h', "host", Required = true, HelpText = "server host")]
            public string Host { get; set; }

            [Option('p', "port", Required = true, HelpText = "server port")]
            public int Port { get; set; }

            [Option('t', "to", Required = true, HelpText = "package name")]
            public string Package { get; set; }

            [Option('s', "push", Required = false, HelpText = "push notify")]
            public bool isPush { get; set; }

            [Option('n', "notify", Required = false, Separator = ',',
                HelpText = "usage: -n[--notify] <title>,<owner>,<icon_url>,<body>")]
            public IEnumerable<string> notify { get; set; }

            [Option('g', "pull", Required = false, HelpText = "pull notify")]
            public bool isPull { get; set; }
        }
        static void Main(string[] args)
        {
            Parser.Default.ParseArguments<Options>(args)
            .WithParsed<Options>(o => RunWithOpts(o))
            .WithNotParsed<Options>(e => ErrorHandle(e));
        }

        static void ErrorHandle(IEnumerable<Error> e)
        {
            foreach (var item in e)
                System.Console.WriteLine(item.StopsProcessing);
        }

        static void RunWithOpts(Options o)
        {
            _host = o.Host;
            _port = o.Port;
            _package = o.Package;

            if (o.isPush)
            {
                if (o.notify == null || o.notify.ToList().Count != 4)
                {
                    System.Console.WriteLine("Fill notify arguments");
                    return;
                }

                var n = o.notify.ToList();
                Notifyp.NotifyQ notify = new Notifyp.NotifyQ()
                {
                    // Title = n[0],
                    // Owner = n[1],
                    // IconUrl = n[2],
                    // Body = n[3]
                };

                Push(notify);
            }
            else if (o.isPull)
            {
                Pull();
            }
        }

        static void Push(Notifyp.NotifyQ notify)
        {
            var channel = new Channel(_host + ":" + _port, ChannelCredentials.Insecure);
            var client = new Notifyp.NotifyServices.NotifyServicesClient(channel);

            System.Console.WriteLine("push n: " + notify.ToString());

            var resp = client.PushNotify(new Notifyp.PushNotifyRequest()
            {
                Notify = notify,
                // To = new Notifyp.AppQ()
                // {
                //     AppPackageId = _package,
                //     AppUserId = "*"
                // }
            });

            System.Console.WriteLine("push resp: " + resp.ToString());
        }

        static void Pull()
        {
            // var app = new Notifyp.AppQ()
            // {
            //     AppPackageId = _package,
            //     AppUserId = "123456789"
            // };

            var channel = new Channel(_host + ":" + _port, ChannelCredentials.Insecure);
            var client = new Notifyp.NotifyServices.NotifyServicesClient(channel);

            // var resp = client.PullNotify(new Notifyp.PullNotifyRequest() { App = app });

            // System.Console.WriteLine("pull n: " + resp.ToString());
        }
    }
}
