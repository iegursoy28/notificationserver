﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace NotificationServer
{
    public class Startup
    {
        private static readonly string AllowAnyOriginPolicy = "AllowAnyOriginPolicy";

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(option =>
            {
                option.AddPolicy(AllowAnyOriginPolicy, policy =>
                {
                    policy.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod();
                });
            });

            services.AddMvc();
            services.AddGrpc();
            services.AddSingleton(new NotificationServerImpl(null));
            services.AddGrpcReflection();
            services.AddResponseCompression(opts =>
            {
                opts.MimeTypes = ResponseCompressionDefaults.MimeTypes.Concat(
                    new[] { "application/octet-stream" });
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            #region Debugging
            app.UseDeveloperExceptionPage();
            app.UseBlazorDebugging();
            #endregion

            #region AppWasm
            app.UseStaticFiles();
            app.UseClientSideBlazorFiles<AppWasm.Program>();
            #endregion

            app.UseRouting();
            app.UseCors(AllowAnyOriginPolicy);

            app.UseGrpcWeb();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGrpcService<NotificationServerImpl>()
                         .EnableGrpcWeb()
                         .RequireCors(AllowAnyOriginPolicy);

                endpoints.MapGrpcReflectionService();

                endpoints.MapDefaultControllerRoute();
                endpoints.MapFallbackToClientSideBlazor<AppWasm.Program>("index.html");
            });
        }
    }
}
