using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using static DBP.DBsProxyService;

namespace NotificationServer
{
    public class NotificationServerImpl : Notifyp.NotifyServices.NotifyServicesBase
    {
        private static string DBPROXY_ENDPOINT = Environment.GetEnvironmentVariable("DBPROXY_ENDPOINT");

        private DBsProxyServiceClient DBClient;

        private readonly ILogger<NotificationServerImpl> _logger;
        private readonly IEG.Logger logger = new IEG.Logger("NotificationServerImpl") { IsLog = false };

        public NotificationServerImpl(ILogger<NotificationServerImpl> logger)
        {
            AppContext.SetSwitch("System.Net.Http.SocketsHttpHandler.Http2UnencryptedSupport", true);
            Grpc.Net.Client.GrpcChannel channel = Grpc.Net.Client.GrpcChannel.ForAddress(DBPROXY_ENDPOINT);
            DBClient = new DBsProxyServiceClient(channel);

            _logger = logger;
        }

        public override Task<Notifyp.PushNotifyResponse> PushNotify(Notifyp.PushNotifyRequest request, ServerCallContext context)
        {
            try
            {
                var ctx = new DBP.DBContext()
                {
                    DbId = request.Ctx.AppId,
                    ContainerId = request.Channel
                };

                var pullReply = DBClient.FetchData(new DBP.FetchDataRequest()
                {
                    Ctx = ctx,
                    DataKey = "*"
                });

                if (pullReply.Contents?.Count <= 0)
                    throw new RpcException(new Status(StatusCode.Cancelled, "Not found node"));

                foreach (var item in pullReply.Contents)
                {
                    if (item.Value.TryUnpack(out Notifyp.NodeQ n))
                    {
                        n.Notifications.Add(request.Notify);

                        // Replace new value
                        DBClient.PutData(new DBP.PutDataRequest()
                        {
                            Ctx = ctx,
                            Data = new DBP.DataContent()
                            {
                                DataKey = item.DataKey,
                                Value = Google.Protobuf.WellKnownTypes.Any.Pack(n)
                            }
                        });
                    }
                }

                return Task.FromResult(new Notifyp.PushNotifyResponse());
            }
            catch (Exception e)
            {
                logger.Error($"{e}", e);
                return Task.FromException<Notifyp.PushNotifyResponse>(e);
            }
        }

        public override Task<Notifyp.PullNotifyRespose> PullNotify(Notifyp.PullNotifyRequest request, ServerCallContext context)
        {
            var ctx = new DBP.DBContext()
            {
                DbId = request.NodeCtx.AppCtx.AppId,
                ContainerId = request.NodeCtx.Node.Channel
            };

            try
            {
                var fetchReply = DBClient.FetchData(new DBP.FetchDataRequest()
                {
                    Ctx = ctx,
                    DataKey = request.NodeCtx.Node.Uuid
                }, deadline: System.DateTime.UtcNow.AddSeconds(2));

                if (fetchReply.Contents?.Count <= 0)
                    throw new RpcException(new Status(StatusCode.Cancelled, "Not found node"));

                var node = request.NodeCtx.Node.Clone();
                foreach (var item in fetchReply.Contents)
                {
                    if (item.Value.TryUnpack(out Notifyp.NodeQ n))
                    {
                        // Put notifications
                        node.Notifications.Add(n.Notifications);

                        // Clear notify list
                        n.Notifications.Clear();
                        // Replace new value
                        DBClient.PutData(new DBP.PutDataRequest()
                        {
                            Ctx = ctx,
                            Data = new DBP.DataContent()
                            {
                                DataKey = item.DataKey,
                                Value = Google.Protobuf.WellKnownTypes.Any.Pack(n)
                            }
                        });
                    }
                }

                return Task.FromResult(new Notifyp.PullNotifyRespose() { Node = node });
            }
            catch (Grpc.Core.RpcException re) when (re.StatusCode == Grpc.Core.StatusCode.NotFound)
            {
                DBClient.PutData(new DBP.PutDataRequest()
                {
                    Ctx = ctx,
                    Data = new DBP.DataContent()
                    {
                        DataKey = request.NodeCtx.Node.Uuid,
                        Value = Google.Protobuf.WellKnownTypes.Any.Pack(request.NodeCtx.Node)
                    }
                });

                logger.Info($"Create node: {request.NodeCtx.Node}");
                return Task.FromException<Notifyp.PullNotifyRespose>(new RpcException(new Status(StatusCode.OK, "Register node")));
            }
            catch (Exception e)
            {
                logger.Error($"{e}", e);
                return Task.FromException<Notifyp.PullNotifyRespose>(e);
            }
        }
    }
}
